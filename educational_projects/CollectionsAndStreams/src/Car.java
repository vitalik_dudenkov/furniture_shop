
public abstract class Car {
	
	private double speed;
	private double fuel;
	private String name;
	
	public Car() {
		this.speed = 0;
		this.fuel = 100;
		this.name = "NoName";
	}
	
	public Car(String name, double fuel) {
		this.speed = 0;
		this.fuel = fuel;
		this.name = name;
	}
		
	
	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		if(speed < 0) {
			this.speed = 0;
		} else {
			this.speed = speed;
		}
	}

	public double getFuel() {
		return fuel;
	}

	public void setFuel(double fuel) {
		this.fuel = fuel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public abstract void drive();
//		this.fuel -= 1;
//		System.out.println("����, �����...");
	
	public void accelerate() {
		this.speed += 10.0;
		System.out.println("������, ��� ����");
	}
	
	public void decelerate() {
		this.speed -= 10.0;
		System.out.println("�� ���-� ��, �����, ���� �����...");
	}
	
	public void refuel(double fuel) {
		this.fuel += fuel;
		System.out.println("�������� ������� ���������");
	}
	
	@Override
	public String toString() {
		return "���������� " + this.name + ". ������� ��������: " + this.speed + "��/�, ������� ������� �������: " + this.fuel + "�.";
	}
	
	@Override
	public boolean equals(Object obj) {
		Car car = (Car) obj;
		return this.name == car.name
				&& this.fuel == car.fuel
				&& this.speed == car.speed;
	}
	
}
