import java.util.ArrayList;
import java.util.List;

public class Starter {

	public static void main(String[] args) {
		
		AssDriver volvo = new CountryCar();
		AssDriver volga = new RacingCar();
//		System.out.println(volga);
//
		
		List<AssDriver> lst = new ArrayList<>();
		lst.add(volvo);
		lst.add(volga);
		
		for(AssDriver d : lst) {
			d.driveAssSlow();
		}
	}

}
