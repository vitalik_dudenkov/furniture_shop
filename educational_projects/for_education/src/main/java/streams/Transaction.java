package streams;

public class Transaction {
    private final String referenceCode;
	private final Trader trader;
	private final int year;
	private final int value;
	
	public Transaction(String referenceCode, Trader trader, int year, int value) {
		super();
		this.referenceCode = referenceCode;
		this.trader = trader;
		this.year = year;
		this.value = value;
	}

	public Trader getTrader() {
		return trader;
	}

	public int getYear() {
		return year;
	}

	public int getValue() {
		return value;
	}
	

	public String getReferenceCode() {
	    return referenceCode;
	}

	@Override
	public String toString() {
		return "Transaction [trader=" + trader + ", year=" + year + ", value=" + value + "]";
	}
}
