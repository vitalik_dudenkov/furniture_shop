package streams;

import static java.util.stream.Collectors.*;
import java.util.*;
import java.util.stream.*;
import streams.Dish.CaloricLevel;

public class Main {
	public static void main(String[] args) {
		// ������� ���� ��� ������, �� ������ �������� ����� ����������� �����������.
		List<Dish> menu = Arrays.asList(
			new Dish("pork", false, 800, Dish.Type.MEAT),
			new Dish("beef", false, 700, Dish.Type.MEAT),
			new Dish("chicken", false, 400, Dish.Type.MEAT),
			new Dish("french fries", true, 530, Dish.Type.OTHER),
			new Dish("rice", true, 350, Dish.Type.OTHER),
			new Dish("season fruit", true, 120, Dish.Type.OTHER),
			new Dish("pizza", true, 550, Dish.Type.OTHER),
			new Dish("prawns", false, 300, Dish.Type.FISH),
			new Dish("salmon", false, 450, Dish.Type.FISH) );
		
		// ������� �� ������ ������ ���� 3 ���������� �����
		List<String> threeHighCaloricDishName = 
				menu.stream().
				filter(p -> p.getCalories() > 300).
				map(Dish::getName).
				limit(3).
				collect(toList());
		System.out.println(threeHighCaloricDishName);
		
		// ��������� ��� ���� ���, ���� ���������� ��� �������, �������������� �������, ��������
		threeHighCaloricDishName = 
				menu.stream().
				filter(d ->{
					System.out.println("filtering " + d);
					return d.getCalories() > 300;
					}).
				map(d ->{
					System.out.println("mapping " + d);
					return d.getName();
				}).
				limit(3).
				collect(toList());
		
		//����������, ��� � ��� ���� 2 ������� ����� [1, 2, 3] � [3, 4]
		// ���������� ������� ������ �������� � �� ��������� �����������
		// �������� [(1, 2), (1, 3), (2, 2)...]
		// �������, ���� ��������� ������ �� ����, ����� ������� ������ ���
		List<Integer> numbers1 = Arrays.asList(1, 2, 3);
		List<Integer> numbers2 = Arrays.asList(3, 4);
		List<int[]> pairs = numbers1.stream()
				.flatMap(i -> numbers2.stream()
						.filter(j -> (i + j) % 3 == 0)
						.map(j -> new int[] {i, j})
						)
				.collect(toList());
		// ��� �������� ����������� ������������� �� ������ �� �������������
		// ������� ������������� ������ ������ ��������
		// � ���� ������ ����
		System.out.println(pairs.get(0)[0] + " " + pairs.get(0)[1]); // 2 4
		
		//��� ������ �������� reduce ����� ��������� "������",
		// �� ���� ���-�� ������� � ���������� ������ � � ���������� ��������, ��������, �����
		Integer sumNums = numbers1.stream().reduce(0, Integer::sum);
		System.out.println(sumNums); // 6
		
		// ��� �� ����� �� ����� �����������
		// reduce(startVal, BinaryOperator<T>) ��� ��������� �������� � �������, ���������� � T � ������������ T
		Integer mulNums = numbers1.stream().reduce(1, (a, b) -> a*b);
		System.out.println(mulNums); // 6
		
		// ����� �� ���������� ������ �������� ������� reduce(), �� ����� ������� ���������� ������� ��� ������� ������
		sumNums = numbers1.stream().reduce(Integer::sum).orElse(0);
		System.out.println(sumNums); // 6
		
		// ��������� ���������� ����
		Integer count = menu.stream().map(d -> 1).reduce(Integer::sum).orElse(0);
		Long cnt = menu.stream().count();
		System.out.println(count); // 9
		System.out.println(cnt); // 9
		
		// ������� ����� � IntStream
		
		// ������� ��� ���������� ������ (��, ��������, � ��������� �� 1 �� 100) � ������ ������ 5
		IntStream.rangeClosed(1, 100).boxed()
			.flatMap(a ->
					IntStream.rangeClosed(a, 100)
						.filter(b -> Math.sqrt(a*a + b*b)%1 == 0)
						.mapToObj(b -> 
								new int[] {a, b, (int) Math.sqrt(a*a + b*b)})
						)
			.limit(5)
			.forEach(arr -> System.out.println(arr[0] + " " + arr[1] + " " + arr[2]));
		
		// � ��� ������� ��������
		IntStream.rangeClosed(1, 100).boxed()
			.flatMap(a ->
					IntStream.rangeClosed(a, 100)
						.mapToObj(b ->
								new double[] {a, b, Math.sqrt(a*a + b*b)})
						.filter(t -> t[2]%1 == 0))
			.limit(5)
			.forEach(arr -> System.out.println(arr[0] + " " + arr[1] + " " + arr[2]));
		
				
		// ��������� ��� ���������� ��������
		// ������� ������ 10 ��������� ����
		Stream.iterate(new int[] {0, 1},
					t -> new int[] {t[1], t[0] + t[1]})
			.limit(5)
			.map(t -> t[0])
			.forEach(System.out::println);
		
		
		// ������� � ���� � �������� �¨���� collect()
		
		//��� ���� ������ ��������� ������� ����� ����
		// (�������, ������ �� ����� ������� ������ ��� �������� �������� ���������� ��������� ���������)
		// ������ ��� ��� ����� ������� �� ������� menu.size(), ������ ���� ������� ��� �������� ���������� ������� ����� ���������
		Long menuSize = menu.stream().collect(counting());
		System.out.println(menuSize);
		
		// ��������� ����� ����� ���������� �����. ������������� maxBy(Comparator<T>)
		// ���� �� ���������, ������� �������� ����� ��� ����������, � ����� ��������� ��� �����������
		Comparator<Dish> dishCaloriesComparator =
				Comparator.comparing(Dish::getCalories); // �� ������������
		Optional<Dish> mostCalorieDish =
				menu.stream()
					.collect(maxBy(dishCaloriesComparator)); // Optional �� ������ ������� ����
		System.out.println(mostCalorieDish); // ����� �� �����
		
		// summingInt ��������� �� ����� �������, ������������ ������ � ����������� �������� (int)
		// ��������� ������ ������������ ����
		System.out.println(menu.stream().collect(summingInt(Dish::getCalories)).doubleValue());
		
		//averagingInt ��������� �� �� ���������, �� �� ���������, � ��������� �������
		System.out.println(menu.stream().collect(averagingInt(Dish::getCalories)).doubleValue());
		
		// � ���� � ����� ���� ��� ���������� ����� �������� � ����� � ������� � ������� � ��������?
		IntSummaryStatistics menuStatistics = menu.stream().collect(summarizingInt(Dish::getCalories));
		System.out.println("�������: " + menuStatistics.getAverage() 
			+ ", �����: " + menuStatistics.getSum() + ", ����������: " + menuStatistics.getCount() 
			+ ", ��������: " + menuStatistics.getMax() + ", �������: " + menuStatistics.getMin());
		System.out.println(menuStatistics); // �� �� �����, �� ������� � ������� ��� �������
		
		// ��... � ���� ������ �� ����� ������� �� ����� ��� �������� ����
		String shortMenu = menu.stream().map(Dish::getName).collect(joining(", "));
		System.out.println(shortMenu);
		
		Map<Dish.Type, Integer> totalCaloriesByType =
				menu.stream()
				.collect(groupingBy(Dish::getType,
						summingInt(Dish::getCalories)));
		System.out.println(totalCaloriesByType);
		
		Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType =
				menu.stream().collect(
						groupingBy(Dish::getType, mapping(dish -> {
						if (dish.getCalories() <= 400) return CaloricLevel.DIET;
						else if(dish.getCalories() <= 700) return CaloricLevel.NORMAL;
						else return CaloricLevel.FAT;
						}, toSet())));
		System.out.println(caloricLevelsByType);
		
		//��������� ������ ��������������� ����������� ����������
		List<Dish> dishes = menu.stream().collect(new ToListCollector<>());
		System.out.println(dishes);
		
		// � ���� �� ������������ ���� ���������, �� ����� � ���
		// ������������� ����� collect ��������� ��� �������:
		// supplier, accumulator, combiner (�� ��, ��� ����� ���� ��� ������������ ����������)
		dishes = menu.stream().collect(
					ArrayList::new,
					List::add,
					List::addAll);
	}
}
