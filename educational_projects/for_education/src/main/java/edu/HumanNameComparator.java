package edu;

import java.util.Comparator;

//��� ��� ����, ���� ���� ����� ��� ������������, ������ �� ��������� ���� ���������
//��� ����������� ����� ������� ������ Human, �� � ����������� �������� ��� ������� Human
public class HumanNameComparator implements Comparator<Human> {
	
	// ���������� ����������, ������� �� ��� ����������� ����� compare
	// ������������ 2 ������� ������� ��� ������
	// ��� ���-�� �� � ������ �������� ����������. �� ������
	@Override
	public int compare(Human h1, Human h2) {
		// ��� ��� ��� ����� compareTo ��� ����������, ��� ���� ���������� �� ��������
		// ������������ ����� � CollectionsWork.java
		return h1.getName().compareTo(h2.getName());
	}
}
