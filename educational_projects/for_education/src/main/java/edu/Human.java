package edu;

import java.util.Arrays;

//������ ����������� �����, ������������� � ��������-��������, �����, ������� ����� �������������� � ����������
//������ �������������� ������ equals � hashCode (� ��� ���������� ���������� ��� � ���������������� ��������� Comparable)
public class Human implements Comparable<Human> {
	private String name;
	private Double height;
	private String[] hobbies;

	public Human() {
	}
	
	//���������� ��� ����� �������� ������ �� �����������, �������� �����������, ����������� ������ �����
	public Human(String name) {
		this.name = name;
		this.height = 160.0;
		this.hobbies = new String[] {"��������"}; //������� ��� 1 ����� �� ���������
	}

	public Human(String name, double height, String[] hobbies) {
		this.name = name;
		this.height = height;
		this.hobbies = hobbies;
	}

	public String getName() {
		return name;
	}

	public Double getHeight() {
		return height;
	}

	public String[] getHobbies() {
		return hobbies;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public void setHobbies(String[] hobbies) {
		this.hobbies = hobbies;
	}

	// ����� ��� ���������� �������� ������������� �������.
	//������ ��������� �������, ������� ���������� ����������, ������ ����� ������ hashCode
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(height);
		//��� ���������� ������������ ������ ���� ������
		result = prime*result + (int) (temp ^ (temp >>> 32));
		result = prime*result + Arrays.hashCode(hobbies);
		result = prime*result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	//����� ���������, � ����� ������� ������� ������ ������� �������, � � ����� ���
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Human other = (Human) obj;
		if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
			return false;
		if (!Arrays.equals(hobbies, other.hobbies))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if(this.hashCode() != other.hashCode()) {
			return false;
		}
		return true;
	}

	// ����� ��������� ��� ���������� ������ ���������� ��������� (��� ��������) �� �������� ������
	//����� ���������� ����� ��������� ���������, ������������ � �� �����.
	//��� �������, ����������������� ��� ��� ����� ����, � ���� ���� �����, �� ���������� ��������� �����.
	//���� �� �������, ��� ������ this "������" o, ���������� ����� ������ 0
	// ���� �� �������, ��� ������������������ ���� ���� �������� �� ����� - ��������� 0
	// ���� �������, ��� o ������ this, �� ������ ������������� �����.
	//��� ��������, 1, 0, -1.
	@Override
	public int compareTo(Human o) {
		if(this.height > o.height) {
			return 1;
		}else if(this.height < o.height) {
			return -1;
		}else {
			if(this.hobbies.length > o.hobbies.length) {
				return 1;
			}else if(this.hobbies.length < o.hobbies.length) {
				return -1;
			}else {
				return 0;
			}
		}
	}

	//���� ���� ������ ������� �� �������������� ��������� ���������, ������� ������ �����.
	@Override
	public String toString() {
		return this.name;
	}
}
