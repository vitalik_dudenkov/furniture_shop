package edu;

import java.util.ArrayList;

public class MyList<E> extends ArrayList<E> {
	@Override
	public String toString() {
		String result = "{ ";
		for(E el : this) {
			result += el.toString() + " ";
		}
		result += "}";
		return result;
	}
}
