package edu;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*
 * ������ ��� ����������� ����
 * https://acmp.ru/index.asp?main=task&id_task=81
 * https://acmp.ru/index.asp?main=task&id_task=284
 * https://acmp.ru/index.asp?main=task&id_task=272
 * https://acmp.ru/index.asp?main=task&id_task=534
 * */

public class ArrayWork {
	/*������� � Java - ������������ ��������� ������ (� ������� �� python)*/
	
	public static void main(String[] args) {
		ArrayWork wk = new ArrayWork();
//		wk.arraysProcessing();
		
//		double[] a = {2, 6, 7, 1, -3, 7, 6};
//		//��������� ���� ����� ����� �������� ����� �� �������������� ��������.
//		System.out.println(wk.average(a));
//		System.out.println(wk.average(2, 3.5, 7, -3.8, 11));
		System.out.println(wk.sumNums());

	}
	
	public void simpleArr() {
		// ������� �������� � ���������� ��������
		int[] a; // ���������� ��������� ����������
		a = new int[5]; // ��������� ������ ��� ������ �� 5 ���������
		//�� ��������� � java ������� �� �������, � ���-�� ���������
		//�������� (Int, double, long � ��.) ������, ����������� ���� (boolean) false, ������� �������� null
		
		for(int i = 0; i < 5; i++) {
			a[i] = i; // ���������� ������� ������� �� 0 �� 4
		}
		
		for(int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}

	}
	
		
	public void maxInArr() {
		//�������� �������� ����������� ���������� � �������
		double max = Double.NEGATIVE_INFINITY; //������������� ���������� ��������� � ����� ��������� ��������� ��������
		double[] a = new double[] {0.0, 1.0, 8.0, 5.0, 3.0, 11.0}; // ���������� � �������� ������� �� ������� ��������� ���������
		for(int i = 0; i < a.length; i++) {
			max = a[i] > max ? a[i] : max; // ��������� �������� ��������
			/*
			 * ������ ����, ��� ������
			 * if (a[i] > max){
			 * 		max = a[i]
			 * }
			 */
		}
		System.out.println(max);
	}
	
	public void reverseArr() {
		int a[] = new int[] {1, 2, 3, 4, 5, 6};
		int n = a.length; // �������� ����� � ��������� ����������, ��� ��� �� ���� ���������, ��� ����� ����� ��������������.
		// ��������, ��� ��� ����, ���� ���������� ������, ���������� ���� �� ��� ��������, ����� �������� ������� �������
		// ��� ������ ���� ��������� ������� ���������� ��������� ����������
		for(int i = 0; i < n/2; i++) {
			int tmp = a[i]; // �������� ������� ������� �� ��������� ����������
			a[i] = a[n - 1 - i]; // ���������� � ������� ������� ������� ������� �� "������" ��������
			a[n - 1 - i] = tmp; // ��������� "�����"
		}
		
		String res = ""; //��� ������� ������������� ���������� ������
		for(int i = 0; i < n; i++) {
			// ������������ ����� ����������� ��� �� ��� � python ��� ������ ��������� +
			// ������ res += ... ������������ res = res + ...
			res += i < n - 1 ? a[i] + " " : a[i]; // ���� ������� �� ���������, � �������� ������ ������������ ��� � ������, � ���� ���������, �� ��� �������.
		}
		System.out.println(res);
	}
	
	public void averArithm() {
		//��������� ����� �������� ��������������� ��������� �������
		int a[] = new int[] {3, 8, -2, 9, 12, -7, 6};
		int n = a.length;
		int sumEl = 0; //���������� ��� ����� ���������
		for(int i = 0; i < n; i++) {
			sumEl += a[i];
		}
		
		double incorrectRes = sumEl / n; //������� �������� �� ��� ������. �������� �������, �� ���� �������� ���������
		/*
		 * ������� ��� � ����������� ������� � java. ��� �������������� ��� ���� ������, � �������� ��������
		 * � ��� �������� �� ��, ��� ��������� �� ����������� � ��������������� ����������
		 * �� ���� ���� � ��� ���� int a = 3; int b = 5; �� a / b ����� ����� 0 (������� ����� �������������, // � python)
		 * ����� �������� ���� ��������, ���� �� ���� �� ��������� ���������� ���� ������������� � double
		 * �� ���� int a = 3; int b = 5; (double) a / b ����� ����� 0.6
		 */
		double correctRes = (double) sumEl / n; //������ ��������� ��� ����� �������
		System.out.println(incorrectRes);
		System.out.println(correctRes);
	}
	
	public void randCard() {
		//������� ������������ ������������� ������� ��������� ��������
		// ��� ���� ������ ���������� ������� (� ���� ������ ����� new �� ������������)
		String[] SUITS = {"Clubs", "Diamonds", "Hearts", "Spades"}; // ������ ��� �����
		String[] RANKS = {"2", "3", "4", "5", "6", "7", "8", "9", "10",
				"Jack", "Queen", "King", "Ace"}; // ������ ��� �������� ����
		int i = (int) (Math.random() * RANKS.length); //Math.random() ���������� ��������� ����� � ��������� [0; 1]
		int j = (int) (Math.random() * SUITS.length);
		System.out.println(RANKS[i] + " of " + SUITS[j]); //������� �������� ��������������� �����
	}
	
	//������������ ������������ ����������� ������ Arrays
	public void arraysProcessing() {
		// ������ �������� �� ����������� ����� ��� ��� ��������� ������ ������ Arrays ��������� ������ � ��������� ����� ������
		Integer[] a = {2, 8, 7, 1, 3, 9, 12, 5}; // �������� ������ ��� ���������� ����������� � ���
		
		System.out.println(Arrays.toString(a)); // ������� �� ����� ������ � ����������� ����
		Arrays.sort(a); // ��������� ������ � ������� �����������
		System.out.println(Arrays.toString(a));
		Arrays.sort(a, Collections.reverseOrder()); // ��������� ������ � ������� �������� (�� �������� ��� �������� ����������)
		System.out.println(Arrays.toString(a));
		
		Integer[] b = {2, 8, 7, 1, 3, 9, 12, 5};
		System.out.println(Arrays.equals(a, b)); // ���������� ��� ������� ����������� (���������� ��������� ������� � python)
		Arrays.sort(b, Collections.reverseOrder());
		System.out.println(Arrays.equals(a, b));
		
		a = new Integer[] {2, 8, 7, 5, 1, 3, 9, 12}; // ��� ���������� ������������ ���������� ������� ��� ����� "�����������", ������ ��� new ��� ������� ������
		System.out.println(Arrays.binarySearch(a,  5)); // ����� �������� ������� ��������� ������
		
		a = new Integer[5];
		Arrays.fill(a, 2); // ��������� ���� ����� ��������� ������ �����-�� ���������, ��������� [0]*n � python
		System.out.println(Arrays.toString(a));
		
		List<Integer> target;
		
		target = Arrays.asList(a); // ��������� �� ���������� ������� � ������ (��������, ����� �������� ������� ������������)
		String res = "";
		// ����� ������ ����� ��������� ����� ��������� � ������ (���� ����������� �����)
		for(int el : target) {
			res += el + " ";
		}
		System.out.println(res.trim());
	}
	
	//������������ ������ � ���������� ������ ����������. ����� �������, ��� �� ��������� ������ (�� ������)
	public double average(double... values) {
		double sum = 0;
		for(double v : values)
			sum += v;
		return values.length == 0 ? 0 : sum / values.length;
	}
	
	public int sumNums() {
		int[] arr = Arrays.stream(new Scanner(System.in).nextLine().split(" ")).
				map(Integer::parseInt).mapToInt(t -> (int) t).toArray();
		int min = 10000;
		int max = -10000;
		for(int i = 0; i < arr.length; i++) {
			if(i % 2 == 0) {
				min = Math.min(arr[i], min);
			}else {
				max = Math.max(arr[i], max);
			}
		}
		return max + min;
	}

}
