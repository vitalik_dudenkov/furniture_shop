package edu;

public class Question<T> {
	private String text;
	private T answer;
	
	public Question(String text, T answer) {
		this.answer = answer;
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public T getAnswer() {
		return answer;
	}

	public void setAnswer(T answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "Question [text=" + text + ", answer=" + answer + "]";
	}
	
	
}
