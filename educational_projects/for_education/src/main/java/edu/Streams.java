package edu;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//� ���� ������ ��������� � Stream API. ���������� �������� ����� � �����, ����� ��������.
/*
* � �������� ������� ��� ���������������� ����������: ���� ��� ������� ������������� ������ Trader � Transaction
* � ����� ��������� �� �������� Transaction (��� ������� � ���������).
* ���������� �������� ������ �� ��������� �������, ��������� ��������� ��������.
* ������ ����� �������� �� ����� ��� ������� �����-���� toString()
* ������ System.out.println(list). ����� ������������ ��� ��� ������������ (�� �� �����������)
* 1) ����� ��� ���������� �� 2011 ��� � ������������� �� �����
* 2) ������� ������ ��������������� �������, � ������� �������� ��������
* 3) ����� ���� �������� �� ��������� � ������������� �� ������
* 4) ������� ������ �� ����� ������� ���������, ���������������� � ���������� ������� (����� ������ ������ String)
* 5) ��������, ���������� �� ���� ���� ������� �� ������ (true/false)
* 6) ������� �� ����� �����(value) ���� ���������� ��������� �� ��������� 
* 7) ����� ������������ ����� ����� ���� ���������� (������ Integer)
* 8) ����� ���������� � ����������� ������ (������ Transaction)
* ������ ��� ������� � ������ streams
* */
public class Streams {

	// � �������� ����� �������������� �� ������� (List)
	public static void main(String[] args) {
		System.out.println("Test buildStream start");

      // �������� ������ �� ��������
      Stream<String> streamFromValues = Stream.of("a1", "a2", "a3");
      System.out.println("streamFromValues = " + streamFromValues.collect(Collectors.toList())); // ���������� streamFromValues = [a1, a2, a3]

      // �������� ������ �� �������
      String[] array = {"a1","a2","a3"};
      Stream<String> streamFromArrays = Arrays.stream(array);
      System.out.println("streamFromArrays = " + streamFromArrays.collect(Collectors.toList())); // ���������� streamFromArrays = [a1, a2, a3]

      Stream<String> streamFromArrays1 = Stream.of(array);
      System.out.println("streamFromArrays1 = " + streamFromArrays1.collect(Collectors.toList())); // ���������� streamFromArrays = [a1, a2, a3]
      
      // �������� ������ �� ���������
      Collection<String> collection = Arrays.asList("a1", "a2", "a3");
      Stream<String> streamFromCollection = collection.stream();
      System.out.println("streamFromCollection = " + streamFromCollection.collect(Collectors.toList())); // ���������� streamFromCollection = [a1, a2, a3]

      // �������� ��������� ������ �� ������
      IntStream streamFromString = "123".chars();
      System.out.print("streamFromString = ");
      streamFromString.forEach((e)->System.out.print(e + " , ")); // ���������� streamFromString = 49 , 50 , 51 ,
      System.out.println();
      
      Collection<Human> men = Arrays.asList(
      		new Human("����", 173, new String[]{"��������"}),
				new Human("����", 180, new String[]{"��������", "���", "���������"}),
				new Human("������", 173, new String[]{"��������", "���"}),
				new Human("����", 190, new String[]{"���������", "���"})
				);
      //���������, ������� � ��� � ��������� �����, ���� 179 �����������
      long cnt = men.stream().filter((e) -> e.getHeight() > 179).count();
      System.out.println(cnt); // 2
      
      // �������� ����� ������, ���� �������� ������ ����� ������ ���� 180. ���������� � MyList<> ��������� ��� �������� ������
      // �� ��������� �������� �� List<>
      List<Human> select = men.stream().filter((e) -> e.getHeight() <= 180).collect(Collectors.toList());
      System.out.println(select.size()); // 3
      
      // � ������ ������� ���, � ���� ������ 1 �����
      select = men.stream().filter((e) -> e.getHobbies().length > 1).collect(Collectors.toList());
      MyList<Human> testList = new MyList<>();
      testList.addAll(select); // ���� �� ���� ��������, ��� ���-�� �������� �� ���, ���������������� �����
      System.out.println(testList); // { ���� ������ ���� }
      
      //��������� ������� ���, ��� �������
      select = men.stream().filter((e) -> Arrays.asList(e.getHobbies()).contains("��������")).collect(Collectors.toList());
      testList.clear();
      testList.addAll(select);
      System.out.println(testList);
      
      //��������� ����� ������ ��������� ���������� ���� Collection<> ��� ������, ���� ������������ ��� ����� get ����.
      // ���� ��� ��������, ������������ ����, ����� �������� � ����� ���������� (����� Map, ��� �� ��������� ��������� Collection)
      List<String> cities = Arrays.asList("�����", "������", "������", "�������", "�����", "������");
      
      // ������ 1 ������� ����� ���������, ���� ��������� �������� ������ - ������ 1
      String first = cities.stream().findFirst().orElse("1");
      System.out.println(first); // �����
      
      // �� ��������, ������ ��������� ������� ��������� ��� 1 (��������� n - 1 ������ ��������� ���������)
      String last = cities.stream().skip(cities.size() - 1).findAny().orElse("1");
      //��������, ��� ��� ������� ������� � ������ ���� � �� ��
      System.out.println(last.equals(cities.get(cities.size() - 1))); // true
      
      // ��������� ����� ������� � ��������� men
      //findFirst() ���������� ��� Optional<Human>, � get() �������� �������� ������ ������ ������ Human
      Human find = men.stream().filter((e) -> e.getName().equals("������")).findFirst().get();
      // ���-�� �������� ���� �������, ������� ������� ��� � �������
      System.out.println(find.getName() + ", ������ " + find.getHeight()); // ������, ������ 173.0
      
      //� ������ ��������� ����� ����, � ���� � ����� ���� ����� �
      select = men.stream().filter((e) -> e.getName().contains("�")).collect(Collectors.toList());
      testList.clear();
      testList.addAll(select);
      System.out.println(testList); // { ���� ���� }
      
      // ������ 2 ������, ������� �� �������
      List<String> fromTo = cities.stream().skip(1).limit(2).collect(Collectors.toList());
      System.out.println(fromTo.get(0) + " " + fromTo.get(1)); // ������ ������
      
      // � ����� ������ ������� ���� ����������. ������� ��������� �� ���. distinct() ������ ��, ��� ��� �����
      List<String> noRep = cities.stream().distinct().collect(Collectors.toList());
      MyList<String> testLstString = new MyList<>();
      testLstString.addAll(noRep);
      System.out.println(testLstString); // { ����� ������ ������ ������� ������ }
      
      //������� � ������� ������ ���������� � ���������� �������� � ��� ��������
      List<String> mapping = cities.stream().map((s) -> s + "_" + s.length()).collect(Collectors.toList());
      testLstString.clear();
      testLstString.addAll(mapping);
      System.out.println(testLstString);
      
      // � ��������� 2 ������, � ������� ���������� ����� ������� ����� �����.
      Collection<String> collection2 = Arrays.asList("1,2,0", "4,5");
      
      //��������� �������������� ��� �����
      // flatMapToInt() ��������� �� ������ �������� ������� ���������
      // �� ����� ������ ������� � � ���� ��������� ������-�������, �������, � ��� ������� ���� ������� ���� 
      // ���������� � ����� � ��� ������� mapToInt ������ �� ����� �����.
      int sum = collection2.stream().flatMapToInt((p) -> Arrays.asList(p.split(",")).stream().mapToInt(Integer::parseInt)).sum();
      System.out.println(sum); // 12
      
      //��������, ��� ������� �������� ��������� �����. ��������� � � ������������� ������
      Collection<String> numbers = Arrays.asList("2", "9", "12", "26", "4");
      int[] nms = numbers.stream().mapToInt(Integer::parseInt).toArray();
      System.out.println(nms[1]); // 9
      
      //� ��� ���� ��������� ��� �� ���� �������. � ����������. ��������� ������
      // 2 4 7 8 9
      //����������� ��, ��� ����, ���� �� ������ ������������ ������.
//      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//      try {
//      	// � ���� ���������, ���� ��� ����� ���������
//      	int[] arr = Arrays.asList(br.readLine().split(" ")).stream().mapToInt(Integer::parseInt).toArray();
//      }catch (IOException e) {
//			e.printStackTrace();
//		}
      
      //����� ������ �������� ��������
      Human highest = men.stream().max((p1, p2) -> p1.getHeight().compareTo(p2.getHeight())).get();
      System.out.println(highest); // ����
      
      //����� ����� ��������� � ������ �� ��������
      String city = cities.stream().max(String::compareTo).get();
      System.out.println(city); // �������
      
      // � ��� ������ �� ��������
      city = cities.stream().min(String::compareTo).get();
      System.out.println(city); // ������
      
      //� ������ �� �� �����, �� �����������
      //�� ����� ��������, ��� ����� ������ ������� �� ������ �� ���������, �� � ������ �� ���� ������
      // ������� ���������� ����� ���� ������� � ���
      // � �� ������ � ������� Arrays � Collections
      city = cities.stream().sorted().distinct().findFirst().get();
      System.out.println(city); //������
      
      //���������� ���������� ��������� (��� �� ������ ���, �������� Comparable � ����� �����)
      // �� ������ ��������� ��������� ��������� ����� � �������� ��������� ���� ����������
      // ������� ��������� �� �����, � ����� � ���������� �������, �� ������.
      // ������� ��������, ��� ��� ������ ����� ���������� �����, ���� ������������ ���� ���� �� ������������.
      // ������ ��� ����� compareTo �������� ������ � ���������� ������.
      select = men.stream().sorted((p1, p2) -> p1.getHeight() != p2.getHeight() ?
      		p1.getHeight().compareTo(p2.getHeight()) : p1.getName().compareTo(p2.getName()))
      		.collect(Collectors.toList());
      testList.clear();
      testList.addAll(select);
      System.out.println(testList); //{ ���� ������ ���� ���� } ����������� ���������. ����� �� �� �������� (������ 2)
      // ������ �����. ������ �������������� �� ���� ���, ������� �������� ������� ������� � ������ �����������
      // �� ���� �� ����� �������, ����� ������������������ ���������� � ������� �� ��������...
      // ����������� ��������, � ��� ���
      
      // � ��� ���� ������� �����? ������������� Comparator.comparing() �� �������� �����.
      // � �����! �������� �� ���!
      select = men.stream()
      		.sorted(Comparator.comparing(Human::getHeight).thenComparing(Human::getName)).
      		collect(Collectors.toList());
      System.out.println(select); // [ ������, ����, ����, ���� ] 
      
      // � ��� ���� ������� �������� ��� �������, ����������� Comparable. ����� ���������� ����� ����������
      // �� �� ����� ��������, ��� ��� compareTo ��������� �� �� �������� �����, � �� ���������� �����
      select = men.stream().sorted().collect(Collectors.toList());
      testList.clear();
      testList.addAll(select);
      System.out.println(testList); // { ���� ������ ���� ���� }

      //������� ���������� � �������������� �����������. � ���� ���� ����� �������� ��������� ���������� �����.
      Collection<Integer> coll = Arrays.asList(1, 2, 3, 4, 2);
      
      //����� ����� ����� ���������  � ���� ������. � ���� ��������� ������, ������ 0
      System.out.println(coll.stream().reduce((s1, s2) -> s1 + s2).orElse(0)); // 12
      
      //����� �������� (��� ������ -1, ���� ��������� ������)
      System.out.println(coll.stream().reduce(Integer::max).orElse(-1)); // 4
      
      //������� ����� ���� �������� �����
      System.out.println(coll.stream().filter((o) -> o % 2 != 0).reduce((s1, s2) -> s1 + s2).orElse(0)); // 4
      
      // � ��� ���� ����������� ����� �� ������ ��������, � ������?
      System.out.println(coll.stream().sorted((p1, p2) -> -1*p1.compareTo(p2)).limit(2).collect(Collectors.toList()).get(1)); // 3
      
      // �������� ��������� ��� ��������� ��������.
      coll = Arrays.asList(1, 2, 3, 4);
      
      //������ ������ ��������� ����� �������� �����
      System.out.println(coll.stream().collect(Collectors.summingInt((p) -> p % 2  == 1 ? p : 0)).doubleValue()); // 4
      
      //������� ������� ��������������
      System.out.println(coll.stream().collect(Collectors.averagingInt((p) -> p)).doubleValue()); // 2.5
      // � ���� �� ������� ����� ������� 1 � ����� �������� �������?
      System.out.println(coll.stream().collect(Collectors.averagingInt((p) -> p - 1)).doubleValue()); // 1.5
      
      //�������� ����� �� ����� � ��������
      Map<Boolean, List<Integer>> map = coll.stream().collect(Collectors.partitioningBy((p) -> p % 2 == 0));
      for(Integer el : map.get(true)) {
      	System.out.print(el + " "); // 2 4
      }
      System.out.println();
      // �� �� ��������, ��� ����, �� � �������������� ������
      map.get(false).forEach(t -> System.out.print(t + " ")); // 1 3
      
      //�������� ������ "������ �� �����������"
      System.out.println();
      List<String> names = Arrays.asList("����", "����", "�������", "������");
      List<Human> someMen = names.stream().map(Human::new).collect(Collectors.toList());
      testList.clear();
      testList.addAll(someMen);
      System.out.println(testList); // { ���� ���� ������� ������ }
      System.out.println(testList.get(0).getHeight()); // �� � ���� ������� ����, ��� �������� ������ �����������. 160.0
      
      //���������� ������������������ �� 10 ��������� ����� � ��������� �� 0 �� 100.
      Double[] randNums = Stream.generate(() -> Math.random()*100).limit(10).toArray(Double[]::new);
      Arrays.asList(randNums).stream().forEach(System.out::println);
	}
}
