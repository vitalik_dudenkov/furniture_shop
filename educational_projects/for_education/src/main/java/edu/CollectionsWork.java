package edu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

/*
 * ��� ������ � ���� ������ ������������ ���������� ����� ��������� ����� ������
 * listWork ������������ � �����-������ ���� ������ � ��������������������, ����������
 * ���� �� �������� � �������������� �������, ��� ���� ����� ������.
 * ��� ���� ����������� � ������� ������ ��� � ���������� ������� ������� ��������� ���������� ��� ��������� ���������
 * ���� ���� ������� � ���� ���� ������� ��� �� �����, ��������� �������, ����������, ����� ��������.
 * */

/*
 * ������������ ������� �� ����� � ����������� �� �����. ������ �� �������.
 * ������� ����� ��������� ���������, ����� ����� ���������� ������������, ��� ����������, ��� ���������
 * �����, ����� ������� ������ ��������� �����, ������� ����� ����������� � ��������� ��� �������
 * ����������� ����������� ���������� � ������� ��� �������� �� �������
 * ����������� ��������� ��������� � ��� ��������� (� ������������� ��� ����������� ������)
 * */
public class CollectionsWork {

	public static void main(String[] args) {
		CollectionsWork cw = new CollectionsWork();
		cw.listWork();
		//cw.setWork();
		//cw.queueWork();
		//cw.mapWork();

	}
	
	//� ������ ������ �������� ������ �� ��������.
	public void listWork() {
		//��������� ������������ ��������� ���������� �� ������ ������ ���� ����� ��������� ��� List
		ArrayList<Integer> a = new ArrayList<>(); //���������� List<Integer> a = new ArrayList<>();
		/*
		 * ��� ��� ���������� �����, ����� ��� �������� ���� ��������� ���������� (List<Integer>), ����� ���
		 * ��� ��������� ���������� (a), ����� �������� ������������, � ����� ��������
		 * (� ����� ������ ��� ������ ������ ArrayList<Integer>, �������� ���� � ����������� �������
		 * ������ �� �������� ������������ ����� ��������.
		 */
		
		//������� ������ ������, ��� ���������� �����, ��� ������ ����� ������������� ������
		//� ������ ���� 2 ���� ������������: ��� ���������� � � 1 �����������. ��� ������ �� ��������� ���������� ���������� ������
		//���� � ����������� ������ �� ������ (��� � �������), �� ������ ����� �������� ��� 10 ���������
		//���� �� ������ �����, �� ��� ����� ���������� ��������� ������ ���������� � �������
		//�������� List<String> lst = new ArrayList<>(1000); ������� ������ �� �� 1000 �����.
		
		//�������� ������ �������
		
		a.add(10); //��������� � ����� ����� ������� 10 --������� ��� ������ {10}
		a.add(8);
		a.add(12); //--������� ��� ������ {10, 8, 12}
		a.add(1, 9); //��������� ����� ������� �� 1 ������� add(pos, value); --��� ������ {10, 9, 8, 12} 
		
		MyList<Integer> b = new MyList<>(); //������ ����������� ���������� ������, ������������� ��� toString()
		// P.S. ������-�� ��� ����� �� ����, ��� �������� ������ ���� ���� toString() ��� ������� (�� �� ������, ��� ������)
		b.addAll(a); //����� ��������� �� ���������� ��������� a � ��������� b (���� ������ ���� ����������)
		System.out.println(b);
		
		//� ���������� ���� �������� � ���������� b (��� ���� ArrayList, �� � ������� �������� �� ����� ��� ������������)
		b.clear(); //������� ���������� ���������
		System.out.println(b);
		
		b.add(5);
		b.addAll(a); // ��� ����� ����������, �������� ����������� ��������� ���������� � ����� (�� �������� ��� ������� ������ ��������)
		System.out.println(b); //--����� ����� {5 10 9 8 12}
		//����� �������, ���� � ��� �� ������ ����� ��������� ������� � ������ ������� ������ ���.
		//���� ��������, ��������, ����� ������� ��������� ������� � ����
		//� �����, ��� ������ addAll() ����� ������� � ������ �� ������ ������ ������, �� � ������ ���������, �������� Set.
		Set<Integer> s = new HashSet<>();
		s.add(23);
		s.add(25);
		s.add(31);
		b.addAll(s);
		System.out.println(b); //--����� ����� {5 10 9 8 12 23 25 31} (��������� �������� ����� ���� �� � ����� �������)
		
		//� ������ ����� ����� �������� �� ������ ������ � �� �������� �� ������, � ����� ������� ��������� ���������.
		b.clear(); //�������
		Collections.addAll(b, 2, 8, 7, 11, 31); //Collections.addAll(collection, values...);
		System.out.println(b); //����� {2 8 7 11 31}
		
		//���� ����� ������������ ������� ����� ������ (���� ����� ������ � ���� �� ����������, ���������� clone()
		MyList<Integer> copyList = (MyList<Integer>)b.clone(); //clone() ���������� Object, ������� ���������� ���������� �����
		System.out.println(copyList); // {2 8 7 11 31}
		
		b.remove(0); //������� ������� �� �������. ��������� {8 7 11 31}
		//b.remove(11); - ������, ����� �������� ����� 11-� ������, �������� ���. �� �������� ����� ����� ����� ����� ����������� �����������.
		b.remove(new Integer(11)); //������� ������ �� ������. ��������� {8 7 31}
		System.out.println(b);
		
		int val = b.get(0); //������ � ������ �������� �������������� �� �������
		System.out.println(val); //����� 8
		
		b.set(0, 13); //������������� ����� �������� ��� �������� ������ �� �������
		System.out.println(b.get(0)); // 13
		
		
		//�� ������ ������ ������ ����� ��� {13 7 31}
		Collections.addAll(b, 7, 9, 2); //������� ��� ���������, ������ ��� {13 7 31 7 9 2}
		
		//����� ���������� ������ ���������� �������� � ����������. ����� ������� ��� ������ � Java?
		System.out.println(b.indexOf(7)); //������� ������ ������� ��������� ��������. ���� ������ ��� - ������ -1
		System.out.println(b.lastIndexOf(7)); //������ ���������� ��������� ��������. ��� ������� -1
		System.out.println(b.contains(31)); // ���������, ���� �� ����� ������� � ���������. ��������� true ��� false
		
		
		//������� �������� ������
		b.clear();
		Collections.addAll(b, 2, 8, 1, 9, 13);
		//1) ������� for
		for(int i = 0; i < b.size(); i++) {
			//������ � ��������� b.get(i);
		}
		
		//2) foreach
		for(Integer el : b) {
			//������ � ��������� ����� ��������� ���������� el
		}
		
		//3) Iterator ��������� �����, ����� ����� ������������� �������������� ������ ����� �� ����� ��������
		//�� ������ �������� �������� ���������, �� � ������� (���������, ��������������, ����)
		ListIterator<Integer> it = b.listIterator(); //{2, 8, 1, 9, 13}
		while(it.hasNext()) {
			Integer tmp = it.next();
			if(tmp % 2 == 0) {
				it.remove();
			}
		}
		System.out.println(b); //{1 9 13}
		
		//��� �� ������� � �������� �������
		String res = "{ ";
		while(it.hasPrevious()) {
			res += it.previous() + " ";
		}
		res += "}";
		System.out.println(res); // {13 9 1}
				
		
		//�������� � ����������. � �������, ������� ����� �������� � ���������. �������� � ���� Human.java
		//� ������ �������� ����� ������. �� �������� ������ Human
		MyList<Human> men = new MyList<>();
		Collections.addAll(men, new Human("����", 173, new String[]{"��������"}),
				new Human("����", 180, new String[]{"��������", "���", "���������"}),
				new Human("������", 173, new String[]{"��������", "���"}),
				new Human("����", 190, new String[]{"���������", "���"}));
		System.out.println(men); //{���� ���� ������ ����}
		
		MyList<Human> forSorts = (MyList<Human>) men.clone(); //��������� �������� ������, ���� ���������� �� ��������� ���
		
		//����������� ��������� � ������� �����������.
		Collections.sort(forSorts);
		System.out.println(forSorts); //{���� ������ ���� ����} ���� � ������ ������������� ��� �� ������� �������� - ���������� �����.
		Collections.sort(forSorts, Collections.reverseOrder());
		System.out.println(forSorts);// {���� ���� ������ ����}
		
		// ����, ���������� ������, ������� � ���� ���������� Comparable �� ����������
		// �� � ���� ����� ��� ������ ������, � ����������� ��� ������� ��-���� ����?
		// ����� �� ������ ��� ����� ����� list.sort(Comparator<? super E> c);
		// ������ �� ������ �������� ����, � ������ ��������.
		// ��, �������, ��� ��� ��� ����� �����-�� ��� ����������, ���������� ��� ������� �������.
		// �������, ����� HumanNameComparator.java (����������� ����� �� ������)
		// ������ ���� �������������, ���������� ��������
		forSorts.sort(new HumanNameComparator()); // ��� � ��
		
		//�� ���-�� �� ������������ ������, ���� ��� �����-�� ����� ���������� �������� �����
		// ����� ���� ���������... ������ �����-��. � ���� ����������� ��������� �����...
		// ��� ��� ���������� ������? ��� �� �����, ������� ����������� ������� ��� ����������� ������� ������
		// ����� ������ ����� ��� new ��� ����������? ���� � ���, ��� java ������, ��� �� �� ������ �����
		// � ���� �� ������ new ����������, ������ ���������� ����� ��� ����������� ��, ��� ��� ����������
		// �� ��� �����, ��� �������� Comparator ������� ���������� ������ compare(). �� ��� ������� ��� ���.
		forSorts.sort(new Comparator<Human>() {
			@Override
			public int compare(Human h1, Human h2){
				return h1.getName().compareTo(h2.getName());
			}
		});
		
		// � ��� � ����� ������ �� ����� ����, �� ����� �����. ������ ������ � ���, ��� ������ ��� ������ ����������.
		// ��� �� ������ ��� ��������� ��������... � ��� ���� � �����, ��� ����� ������ �� ������������ ������� ����������?
		// � ������ ��� ������� ������ ����������, ��� ������ � �� �����? �������� ���
		forSorts.sort((Human h1, Human h2) -> h1.getName().compareTo(h2.getName()));
		
		// ��� �� ��� ������� ������ ����? ���� 2 �������� Human � ��������� �� �� ������.
		// ���, ������, ����� ��������, ������ ��� java ���� �����, � ��� ��������, ������ ���������
		forSorts.sort((h1, h2) -> h1.getName().compareTo(h2.getName()));
		
		// � ���� � �����, ��� � ��� �� ����� �������� �������?
		// � ���������� ���������� ���� ��� ����������� ����� (� �����������), �� �� ���
		// ������ �� ����� (��� ������-���������), ���������� ��������� ����������
		// ��� ���������� �� ������ ���������� ����� ���������
		forSorts.sort(Comparator.comparing(Human::getName));
		System.out.println(forSorts); // { ���� ������ ���� ���� }
		
		// � ���� �� �� ����� ����� ������������� � �������� �������... ���������� �������� ��������.
		forSorts.sort(Comparator.comparing(Human::getName).reversed());
		System.out.println(forSorts); // { ���� ���� ������ ���� }
		
		// �� � ��������� �� ���� ���� (���������� ���� � ����������). ���������� �� ���������� ���������
		// ���� ��� ��� ����� ����� �� �������� ��������������� ����������� Comparable � ������
		// ��������� ������������� ������ ��� ���������� �������� ������� �� �����, � ����� �� ��������
		forSorts.sort(Comparator.comparing(Human::getHeight).thenComparing(Human::getName));
		System.out.println(forSorts); // { ������ ���� ���� ���� } ������ � ���� ����� �� �����, �� ������ ������ �� ��������
		
		//� ���� �� ������ �������� ��������� �� �����������, � �� ������� �� �������� ��� ��������?
		// ��, ��������, �� ����� �� ��������, � �� ����� �� ��������.. ����� ���
		forSorts.sort(Comparator.comparing(Human::getHeight).reversed()
				.thenComparing(Human::getName));
		System.out.println(forSorts); // { ���� ���� ������ ���� }
		
		//��� ��� ��� ���������� ������� �� �������� ���� � ��������� ������ �����
		// � ���� � ���� ������ (�������, ��������, �������� �������)
		// ��������� � ������� �� ������ ��������� � ���������� �����
		// � ������� ���������� � Streams.java (���������� ���� ������ ����� �������� ��������� ����� �����)
		
		// ��� ������
		
		//�������� ������ ���� � ���� ��� 1 ������� ����� ��������� � �������� equals � hashCode
		Human petya = new Human("����", 173, new String[]{"��������"});
		System.out.println(men.get(0).hashCode());
		System.out.println(petya.hashCode());
		System.out.println(petya.equals(men.get(0))); // true
		
		//��� ������ ������� � ��������� ����������, � ����� � �����, �������. ���� ��� ��������.
		Collections.binarySearch(b, 13); // 2
		b.indexOf(13); // 2
		b.contains(13); // true
		
		//� ���� ��������� ����� ����� ������ � ��������� �� ������������ ���������� ���������? ��������, ��������.
		//��� ����� ����������� ����������� � Java 8 Java Stream API. � ��� � ����� Streams.java
		// ���������� � ���� ���� ������ ����� ������������ � ������� ������ � ����������� ���������.
		
		//���������� ������ � ����������
		// �����������, ��� ������ ����� �����
		/*
		 * 2
		 * 1 3
		 * � ������ ������� ���������� ���������, � �� ������ ��� ����, ����� ������
		 */
		// ���� ����������� �������� ����������� ������� � ���������� �������
		// ���� �����-�� ����� ����������� ��������� ������ � ���������� ����� ������������ ��
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		MyList<Integer> l = new MyList<>();
		
		// ����������� ������� ���������� �� ����� ������, ������� ��� ��������, ��������� � ���, ���������� � ���� try--catch (��� try--except � ������)
		//try--catch �������� ������ ��������� (��� ����, �������� �������� ��� �������), ������� ����������, ����������� ������ ����, ������� �� �����
		// ����� � ����������� ����� ���� �������� ����� �����, �������� �� ��������� ��, � ���������������� ����� ������. (��� ������� � ����������)
		try {
			int n = br.read();
			br.readLine();
			int[] arr = br.readLine().chars().filter(Character::isDigit).map(Character::getNumericValue).toArray();
			for(int el : arr) {
				l.add(el);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(l);
				
		/*
		 * ���� ���� �����������, ���������
		 * 4 - ���������� (����� ��������)
		 * 3
		 * 4
		 * 6
		 * 7
		 *  �������� ����������.
		 * */
		l.clear();
		try {
			int n = Integer.parseInt(br.readLine());
			for(int i = 0; i < n; i++) {
				l.add(Integer.parseInt(br.readLine()));
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(l);
		
		//�� � ������� ������ � �����������. ����� subList()
		// ��������, ��� ��������� � ������ b
		System.out.println(b); // { 1 9 13 }
		ListIterator<Integer> iter =  b.subList(1, 3).listIterator();
		while (iter.hasNext()) {
			iter.set((int)Math.pow(iter.next(), 2)); //������� ������ ������� ����� ������������ � �������
		}
		System.out.println(b); // {1 81 169 }
		
		//������ LinkedList �������� ��������� � ArrayList. �� ���� ���� �����������.  LinkedList ��������� �� ������  ��������� List,
		//�� � ��������� Deque (�������, � ���� �������, ��������� ��������� Queue).
		Queue<String> linkList = new LinkedList<>();
		Collections.addAll(linkList, "�����", "�����", "������", "����");
		/*
		 * �� LinkedList ����� ������������ ����,
		 *  �������, ��� ������� �������, �� �������� ������� O(1).
		 * �� ������� � �������� �� �������� ������, ��������� �������� �� ������� ��� ��������
		 *  ����������� �������� ����� O(n).
		 * ������, �� ���������� � �������� �� �������� ������, ��������� ListIterator.add()
		 *  � ListIterator.remove(), ����������� O(1).
		 * ��������� ��������� ����� �������� � ��� ����� � null.
		 */
		System.out.println("--------LinkedList----------");
		res = "";
		for(String el : linkList) {
			res += el + " ";
		}
		System.out.println(res);
		linkList.offer("�������"); //��������� ������� � ����� �������.
		System.out.println(linkList.peek()); // ���������� ������ ������� �������, �� �� ������� ���. (null, ���� �����)
		System.out.println(linkList.poll()); // ���������� ������ ������� ������� � ��� �� ��� �������. (null, ���� ������� �����)
		linkList.element(); // ���������� �� peek() ������ ���, ��� ��� ������ ������� ������� ����������
		linkList.remove(); // ���������� �� poll() ������ ���, ��� ������� ���������� ��� ������ �������
	}
	
	//����� ������� � ����������
	public void setWork() {
		//������ ��������� ���������� ���� �����, ����� ��������� ���������� ������������ �������.
		// ������ � ���������� HashSet. Ÿ �������� ��������������. ������������ ����������� ������� hashCode ������� ������������ �������
		// ������� ��� �������������� ���� ��������� � ����������� ������ hashCode ������ ���� ��������� �������������
		// � �����, ������ ������ ������ �� ����������, �� ����������� ������� �� �������.
		HashSet<String> cities = new HashSet<>();
		Collections.addAll(cities, "�����", "������", "�����", "���������");
		
		boolean canAdd = cities.add("�����");
		System.out.println(canAdd); //false. ������ �����. ����� ���������� ������� �� �������� ���, ��� � ���������, � ������ �� �����������
		
		boolean canRemove = cities.remove("�����");
		System.out.println(canRemove);// true
		//��������, ������������� �� ������� ��������. ������ ��������� ��� 4, ������ ������ ����� 3
		System.out.println(cities.size()); // 3
		
		// ��� ����� ���������� ���������? �� ��� �� ��� � ������
		for(String el : cities) {
			System.out.println(el);
		}
		
		System.out.println("-----------------------");
		
		
		for(Iterator<String> it = cities.iterator(); it.hasNext();) {
			System.out.println(it.next());
		}
		// �� �, ��� �� ������ � �������, �������� ����� �������� ������������ ��������� (����������� �������)
		
		//������� �� � ��������� ����� ���������� � ����� ������, ������ �������
		ArrayList<String> lst = new ArrayList<>();
		Collections.addAll(lst, "������", "�������", "���������", "�����", "������", "�������", "�����");
		cities.addAll(lst); //������ ������ ������ ���� �������, � ����� ���� ��������, ����������� � ���������� ���������.
		//�������� �� ���������, ��� � ����� ����������.
		System.out.println("-----------------------");
		for(Iterator<String> it = cities.iterator(); it.hasNext();) {
			System.out.println(it.next());
		}
		
		//�� �, ������� ��, ���� ������ ������, ��� ��� �������� ����� ��������� �� ��������� �� ����� ���������� ������.
		// P.S. ����� ������� ��������� ����� �� ������ ���������, �� � ������
		HashSet<String> cities2 = new HashSet<>();
		Collections.addAll(cities2, "��������", "�������", "��������");
		System.out.println(Collections.disjoint(cities, cities2)); //true
		cities2.add("�����");
		System.out.println(Collections.disjoint(cities, cities2)); // false
		
		//LinkedHashSet ������ �������� � ������� ����������
		LinkedHashSet<String> lcities = new LinkedHashSet<>();
		lcities.add("�����");
		lcities.add("������");
		lcities.add("�������");
		System.out.println("-----LinkedHashSet-----");
		for(String el : lcities) {
			System.out.println(el); //������� ��������� (�����, ������, �������)
		}
		
		//��������� ������ � ���������� � �������� ������, ���������� � ���������, �� ��, ��� � ��� HashSet
		
		// ��������� ���������� TreeSet (���������� ���������� SortedSet<E>)
		// � ���� ���������� �������� �������� �� � ������� ����������, � � ��������������� �� ����������� ����
		// ��� ����������� �� �����, ������� �������� ����� ��������� � ����� ��������� ����������� �������������
		// ��������� Comparable � ����� compareTo ��� ���������� ����������
		// �������� ������ � ������� ������ �� ��. ������� ���� ������� ������������
		SortedSet<String> animalSet = new TreeSet<>();
		animalSet.add("Antilope");
		animalSet.add("Fox");
		animalSet.add("Goat");
		animalSet.add("Dog");
		animalSet.add("Elephant");
		animalSet.add("Bear");
		animalSet.add("Hippo");
		animalSet.add("Cat");

		Iterator iterator = animalSet.iterator();
		System.out.println("------TreeSet------");
		while(iterator.hasNext()){
		    // Antilope Bear Cat Dog Elephant Fox Goat Hippo
		    System.out.println(iterator.next() + " ");
		}

		System.out.println("----------------------------");
		System.out.println(animalSet.subSet("Dog", "Hippo").toString()); // [Dog, Elephant, Fox, Goat]
		System.out.println(animalSet.tailSet("Dog").toString()); // [Dog, Elephant, Fox, Goat, Hippo]
		System.out.println(animalSet.headSet("Dog").toString()); // [Antilope, Bear, Cat]
		System.out.println(animalSet.first()); // Antilope
		System.out.println(animalSet.last()); // Hippo
	}
	
	public void queueWork() {
		Deque<String> stack = new ArrayDeque<>();
		Deque<String> queue = new ArrayDeque<>();		
        stack.push("A"); //�������� ������� � ����� �����
        stack.push("B");
        stack.push("C");
        stack.push("D");
        while (!stack.isEmpty()) {
            System.out.print(stack.pop() + " "); // ������ ������� �� ������ ������ LIFO(Last In - First Out)
        }
        // ����� D C B A
        // �������� � ��� ��� � �������� ���� �����. ��� ������.
        System.out.println();

        queue.add("A");
        queue.add("B");
        queue.add("C");
        queue.add("D");
        while (!queue.isEmpty()) {
        	System.out.print(queue.remove() + " ");
        }
        System.out.println();
        
        //������ ������� � PriorityQueue. �������� �������� � ������������ ������� (�� ��������� � ������� �����������, �������� ������������� Comparable)
        // �� ����� �������� ������� (�� ��������, ��������) ���������� ����������. ������ ����
        Queue<String> queue1 = new PriorityQueue<>();
        queue1.offer("����");
        queue1.offer("�������");
        queue1.offer("�����");
        queue1.offer("���������");
        System.out.print("Priority queue � Comparable: ");
        while (queue1.size() > 0) {
            System.out.print(queue1.remove() + " ");
        }
        System.out.println();

        PriorityQueue<String> queue2
                = new PriorityQueue<>(4, Collections.reverseOrder()); //� ���� ������������ �������� � ��������� ���������� ��������� � ������� ����������
        queue2.offer("����");
        queue2.offer("�������");
        queue2.offer("�����");
        queue2.offer("���������");
        System.out.print("Priority queue � Comparator: ");
        while (queue2.size() > 0) {
            System.out.print(queue2.remove() + " ");
        }
    }
	
	public void mapWork() {
		/*
		 * ��� ��� � LinkedHashMap � HashMap ����������� ��� ��������, � TreeMap �� ���� ���������� TreeSet, ���������� ���� 1 ���������� Map
		 * HashMap
		 */
		//��� ����������� ���������� ���� �����-���������� �������
		//������ � ����������� ������� ��� ��� �����, ������ - ��� ��������.
		Map<String, Integer> map = new HashMap<>();
		//put(key, val); ��������� ������� val �� ����� key
		map.put("������", 12_692_000); //�������� ������ ����� (����� ������ �������������) ���������
		map.put("�����", 1_054_000);
		map.put("����", 350_000);
		map.put("������", 450_000);
		
		System.out.println(map.get("�����")); // 1054000. ������ �� ����� ������� (������� ��������� �� �����������)
		
		//������� �������� ��������� ����� ���������.
		//1) ��� ������ ��������� (���������� ��������� Set ������
		for(Iterator<String> it = map.keySet().iterator(); it.hasNext();){
			System.out.print(map.get(it.next()) + " ");
		}
		System.out.println();
		
		//�� �, �����������, �������� ��������� ������� �������� ����� �� ����� ��������
		for(Iterator<String> it = map.keySet().iterator(); it.hasNext();){
			String key = it.next();
			if(key.equals("�����")) {
				it.remove();
			}else {
				System.out.print(map.get(key) + " ");
			}
		}
		System.out.println();
		System.out.println(map.size()); // 3. ��������, ��� ������� ������������ �� ��������� ������
		map.put("�����", 1_054_000); // ������
		
		//2 ������. � �������������� ���������� Entry
		for(Entry<String, Integer> entry : map.entrySet()) {
			System.out.print(entry.getKey() + ":" + entry.getValue() + " ");
		}
		System.out.println();
		// ����� ������� ����� ������ ������ �������� �� ����� � ���������
		for(Entry<String, Integer> entry : map.entrySet()) {
			if(entry.getKey().equals("�����")) {
				entry.setValue(entry.getValue() + 1000); //��������� ��������� �����
			}
			System.out.print(entry.getKey() + ":" + entry.getValue() + " ");
		}
		
		System.out.println();
		//���������, ����������, ����� � �������� ������ Map
		for(Integer el : map.values()) {
			System.out.print(el + " ");
		}
		System.out.println();
		//����� ���������.
		//� ������ ��������� ����� ������ ��� �����, ��� � ��������. ��������, ������, ���� �� � ����� ��������� ���������� � ������������
		System.out.println(map.containsKey("�����������"));
		//��������. ���� �� � ��������� ����� � ���������� 350_000
		System.out.println(map.containsValue(350_000)); // true
		
		//���������� ��������� �������� ����������� � ������, ��� ������ ������ �� ��������.
		// �������������� ����������� ������ � ����������� ������������ � ����� Streams.java 
	}
}
